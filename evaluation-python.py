from tkinter.messagebox import *
from tkinter import *
from tkinter import ttk

#######################
###### Fonctions ######
#######################

# Récupération du fichier texte et création d'une liste
with open('10k-most-common.txt') as password_txt: # Ouvre le fichier en insérant l'ensembles des lignes dans password_txt
    lines = password_txt.readlines() # Transformation des lignes sous la forme d'une liste
    lines = [line.strip() for line in lines] # Suppression des retours chariots

# Recherche le rang du mot de passe saisie
def verify_password():
    phrase = ""
    password = value.get() # Récupère la valeur saisie dans le input texte
    password = password.lower() # Transformation de la chaîne de caractères en minuscules
    if password:
        if check_verify_characters.state(): # Vérifier si la checkbox est validé
            for list_passwords in lines: 
                if password in list_passwords: # Vérifier si un mot de passe est bien renseigné
                    phrase = '%s "%s" au rang %s \n' %(phrase,list_passwords,lines.index(list_passwords)+1)
            text = "Votre mot de passe {} est présent dans tout c'est mots de passe : \n{}" # Message si le mot de passe est présent
            showinfo('Information', text.format(password, phrase)) # Affichage du message sous forme d'information
        else:
            if password in lines: # Vérification de la présence du mot de passe dans la liste
                text = "Le mot de passe {} est à la position {} des mots de passe les plus utilisés" # Message si le mot de passe est présent
                showinfo('Information', text.format(password, lines.index(password))) # Affichage du message sous forme d'information
            else:
                showinfo('Information', 'Le mot de passe que vous avez saisie n\'est pas dans la liste') # Affichage du message si le message n'est pas présent
    else:
        showerror('Erreur','Vous n\'avez pas renseigné de mot de passe') # Affichage si il n'y a pas de mot de passe renseigné

def effacer_password():
    contenu = value.get()
    contenu = contenu.lower()
    if contenu:
        entry.delete(0, 'end') # Effacer le contenu de l'entry et le réinitialiser à 0
    else:
        showerror('Erreur','Vous n\'avez pas renseigné de mot de passe')

#######################
###### Interface ######
#######################

# Création de l'interface de saisie et de recherche
frame = Tk()
frame.title('Vérification de mot de passe')
frame.geometry('430x365')
frame.minsize(430, 365)
frame.maxsize(430, 365)
frame.iconbitmap('password.ico')

# Création d'un canvas et intégration d'une image
height = 236
width = 420
image = PhotoImage(file='password_image.png')
canvas = Canvas(frame, width=width, height=height, bd=0, highlightthickness=0)
canvas.create_image(width/2, height/2, image=image)
canvas.grid(row=0, column=0, pady=5, padx=5, columnspan=3)

# Intégration d'un label
label = Label(frame, text="Mot de passe :", width = 10, anchor=E, justify=RIGHT)
label.grid(row=1, column=0)

# Intégration d'un input de type texte dans l'interface
value = StringVar()
entry = Entry(frame, textvariable=value, width=25)
entry.grid(row=1, column=1, pady=5)

# Intégration d'une checkbox
check_verify_characters = ttk.Checkbutton(frame, text="Vérifier votre mot de passe !")
check_verify_characters.grid(row=2, column=0, columnspan=3, pady=5, padx=5, sticky=W+E+N+S)

# Intégration d'un bouton dans l'interface avec comme action le lancement de la méthode de recherche
password_button=Button(frame, text="Rechercher", command=verify_password)
password_button.grid(row=1, column=2)

effacer_button=Button(frame, text="Effacer", command=effacer_password)
effacer_button.grid(row=3, column=0, columnspan=3, padx=5, sticky=W+E+N+S)

# Intégration d'un bouton pour quitter l'interface ou annuler la saisie
exit_button=Button(frame, text="Fermer", command=frame.quit)
exit_button.grid(row=4, column=0, columnspan=3, padx=5, sticky=W+E+N+S)

# Affichage de l'interface
frame.mainloop()